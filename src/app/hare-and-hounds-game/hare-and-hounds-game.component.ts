import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../player/player.service';
import { Player } from '../player/player.interface';
import * as _ from 'lodash';

@Component({
  selector: 'app-hare-and-hounds-game',
  templateUrl: './hare-and-hounds-game.component.html',
  styleUrls: ['./hare-and-hounds-game.component.scss']
})
export class HareAndHoundsGameComponent implements OnInit {

  protected readonly NUMBER_ORDERS = [20, 1, 18, 4, 13, 6, 10, 15, 2, 17, 3, 19, 7, 16, 8, 11, 14, 9, 12, 5];


  public harePlayerInput = '0';


  public players: Player[];

  // The index of the hare player.
  public harePlayer = 0;

  // Current player index value
  public currentPlayer = 0;

  public playerPositions = {};


  public currentDartboardState = {};


  public winningPlayer?: number;


  constructor(public readonly playerService: PlayerService) { }

  ngOnInit() {
    this.players = this.playerService.getPlayers();
    this.resetGame();
  }


  public resetGame() {
    this.harePlayer = parseInt(this.harePlayerInput, 10);
    this.currentPlayer = this.harePlayer;
    this.playerPositions = _.fromPairs(_.range(this.players.length).map(v => {
      return [v, -1];
    }));
    this.playerPositions[this.harePlayer] = 0;
    this.currentDartboardState = this.generateDartboardStateForPosition(0);
    this.winningPlayer = undefined;
  }


  public valueClicked(valueString: string) {
    if (this.winningPlayer) {
      return;
    }

    const valueNumber = parseInt(valueString.substring(1), 10);
    const currentPlayerPosition = this.playerPositions[this.currentPlayer];
    const nextNumber = this.NUMBER_ORDERS[(currentPlayerPosition + 1) % this.NUMBER_ORDERS.length];

    if (valueNumber === nextNumber) {
      this.playerPositions[this.currentPlayer]++;
      if (this.checkIfPlayerHasWon(this.currentPlayer)) {
        this.winningPlayer = this.currentPlayer;
        alert(`${this.players[this.currentPlayer].name} has won!!!`);
        return;
      }
    }

    this.rotatePlayer();
  }


  protected checkIfPlayerHasWon(playerIndex: number) {
    const harePosition = this.playerPositions[this.harePlayer];
    if (playerIndex === this.harePlayer) {
      return harePosition === 20;
    } else {
      return this.playerPositions[playerIndex] >= harePosition;
    }
  }

  protected rotatePlayer() {
    this.currentPlayer = (this.currentPlayer + 1) % this.players.length;
    this.currentDartboardState = this.generateDartboardStateForPosition(this.playerPositions[this.currentPlayer]);
  }


  /**
   * Given a player in a position generate the state to highlight the targets.
   * @param position The index position (5 = -1, 20 = 0, 1 = 1, 18 = 2, etc) of the player.
   */
  protected generateDartboardStateForPosition(position: number) {
    const nextNumber = this.NUMBER_ORDERS[(position + 1) % this.NUMBER_ORDERS.length];
    const state = {};
    state[`s${nextNumber}`] = 'selected';
    state[`d${nextNumber}`] = 'selected';
    state[`t${nextNumber}`] = 'selected';
    return state;
  }

}
