import { Component, OnInit } from '@angular/core';
import { PlayerService } from './../player/player.service';

@Component({
  selector: 'app-game-selection',
  templateUrl: './game-selection.component.html',
  styleUrls: ['./game-selection.component.scss']
})
export class GameSelectionComponent implements OnInit {


  public newPlayerName: string;


  constructor(public readonly playerService: PlayerService) { }

  ngOnInit() {
  }



  createPlayer() {
    this.playerService.createPlayer(this.newPlayerName);
    this.newPlayerName = '';
  }

  deletePlayer(name: string) {
    this.playerService.deletePlayer(name);
  }

}
