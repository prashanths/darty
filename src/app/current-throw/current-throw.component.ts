import { Component, Input } from '@angular/core';
import { parseValueString } from './../helpers';

@Component({
  selector: 'app-current-throw',
  templateUrl: './current-throw.component.html',
  styleUrls: ['./current-throw.component.scss']
})
export class CurrentThrowComponent {

  @Input() disabled: boolean;
  @Input() throws: string[];
  @Input() undoThrow: () => void;
  @Input() confirmThrows: () => void;

  public readonly parseValueString = parseValueString;

}
