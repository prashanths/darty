import { Component, OnInit, AfterViewInit, OnDestroy, Output, EventEmitter, ApplicationRef } from '@angular/core';

@Component({
  selector: 'app-speech',
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.scss']
})
export class SpeechComponent implements OnInit, AfterViewInit, OnDestroy  {

  @Output() valueParsed = new EventEmitter();

  public active = false;
  protected recognition: any;

  constructor(protected readonly ref: ApplicationRef) { }

  ngOnInit() {
    // @ts-ignore
    this.recognition = new window.webkitSpeechRecognition();
    this.recognition.lang = 'en-au';
    this.recognition.continuous = false;
    this.recognition.interimResults = false;
    this.recognition.maxAlternatives = 1;
  }


  ngAfterViewInit() {

    this.recognition.onstart = () => {
      console.log('started recognition');
      this.active = true;
    };

    this.recognition.onresult = (event) => {
      if (event.results.length < 1) {
        return;
      }
      const result = event.results[0][0].transcript;
      const scores = this.parseToScores(result);

      console.log(result, ' ==> ', scores);
      scores.map(score => {
        this.valueParsed.emit(score);
        this.ref.tick();
      });
    };

    this.recognition.onend = () => {
      console.log('stopped recognition');
      this.active = false;
    };
  }


  ngOnDestroy(): void {
    this.recognition.stop();
  }



  public startRecognition() {
    this.recognition.start();
  }



  private parseToScores(transcript: string): string[] {
    const parts = transcript.split(' and ');
    return parts.map(part => {
      return this.parseToScore(part);
    }).filter(v => v !== null);
  }


  private parseToScore(part: string): string | null {
    part = part.toLowerCase();

    // a miss...
    if (part === 'miss') {
      return 's0';
    }

    // plain score notation (miss or single or bullseye)
    const numberMatch = parseInt(part, 10);
    if (!isNaN(numberMatch)) {
      if (numberMatch === 0) {
        return 's0';
      } else if (numberMatch > 0 && numberMatch <= 20) {
        return `s${part}`;
      } else if (numberMatch === 25) {
        return 'b25';
      } else if (numberMatch === 50) {
        return 'b50';
      }
    }

    // single/double/triple
    const scoreMatch = part.match(/(single|double|triple)?\s(\d+)/i);
    if (scoreMatch !== null) {
      let multiplier: string;
      switch (scoreMatch[1]) {
        case 'single': multiplier = 's'; break;
        case 'double': multiplier = 'd'; break;
        case 'triple': multiplier = 't'; break;
      }
      return `${multiplier}${scoreMatch[2]}`;
    }


    // bullseye
    if (part === 'bullseye') {
      return 'b50';
    } else if (part === 'outer bullseye') {
      return 'b25';
    }

    return null;
  }



}
