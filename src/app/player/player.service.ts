import { Injectable } from '@angular/core';
import { Player } from './player.interface';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {


  protected players: Player[];


  constructor() {
    this.loadData();
  }


  public getPlayers(): Player[] {
    return this.players.slice(0);
  }

  /**
   * Creates and persists a new player.
   * @param name The name of the player.
   */
  public createPlayer(name: string) {
    // Don't know how to handle existing name yet... for now just warn and do nothing.
    if (this.players.filter(p => p.name === name).length > 0) {
      console.error('A player with this name already exists.');
      return;
    }
    const player: Player = { name, };
    this.players.push(player);
    this.saveData();
  }


  public deletePlayer(name: string) {
    this.players = this.players.filter(p => p.name !== name);
    this.saveData();
  }


  protected loadData() {
    try {
      const storageData = localStorage.getItem('players') || '[]';
      this.players = JSON.parse(storageData);
    } catch (e) {
      console.error('Failed to fetch/decode localStorage data.', e);
      this.players = [];
      this.saveData();
    }
  }

  protected saveData() {
    try {
      localStorage.setItem('players', JSON.stringify(this.players));
    } catch (e) {
      console.error('Failed to save localStorage data.', e);
    }
  }


}
