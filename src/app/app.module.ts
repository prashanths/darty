import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DartboardComponent } from './dartboard/dartboard.component';
import { BasicGameComponent } from './basic-game/basic-game.component';
import { GameSelectionComponent } from './game-selection/game-selection.component';
import { QuicknavComponent } from './quicknav/quicknav.component';
import { HareAndHoundsGameComponent } from './hare-and-hounds-game/hare-and-hounds-game.component';
import { KnockoutGameComponent } from './knockout-game/knockout-game.component';
import { CurrentThrowComponent } from './current-throw/current-throw.component';
import { SpeechComponent } from './speech/speech.component';

@NgModule({
  declarations: [
    AppComponent,
    DartboardComponent,
    BasicGameComponent,
    GameSelectionComponent,
    QuicknavComponent,
    HareAndHoundsGameComponent,
    KnockoutGameComponent,
    CurrentThrowComponent,
    SpeechComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(fas);
  }
}
