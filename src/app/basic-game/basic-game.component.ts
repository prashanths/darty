import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { PlayerService } from '../player/player.service';
import { Player } from '../player/player.interface';
import { parseValueString } from './../helpers';

@Component({
  selector: 'app-basic-game',
  templateUrl: './basic-game.component.html',
  styleUrls: ['./basic-game.component.scss']
})
export class BasicGameComponent implements OnInit {

  // TODO: Remove this hack once refactored.
  public readonly parseValueString = parseValueString;

  public targetScoreInput = 501;


  public players: Player[];
  public targetScore = 501;

  public currentPlayer = 0;
  public playerScores: any;


  /** Scores for the current go. */
  public dartScores = [];


  public winningPlayer?: number;


  constructor(public readonly playerService: PlayerService) { }

  ngOnInit() {
    this.resetGame();
  }


  public resetGame() {
    this.players = this.playerService.getPlayers();
    this.targetScore = this.targetScoreInput;
    this.currentPlayer = 0;
    this.dartScores = [];
    this.winningPlayer = undefined;
    this.playerScores = _.fromPairs(_.range(this.players.length).map(v => {
      return [v, this.targetScore];
    }));
  }


  public get dartScoresSum() {
    return _.sum(this.dartScores.map(v => parseValueString(v)));
  }


  protected hasCurrentPlayerWon() {
    return (this.playerScores[this.currentPlayer] - this.dartScoresSum) === 0;
  }



  public undoThrow = () => {
    this.dartScores = this.dartScores.slice(0, this.dartScores.length - 1);
  }

  public confirmThrows = () => {
    const roundScore = this.dartScoresSum;
    if (this.playerScores[this.currentPlayer] - roundScore >= 0) {
      this.playerScores[this.currentPlayer] -= roundScore;
    }
    this.dartScores = [];
    this.currentPlayer = (this.currentPlayer + 1) % this.players.length;
  }


  public valueClicked(valueString: string) {
    if (this.winningPlayer) {
      return;
    }

    if (this.dartScores.length === 3) {
      this.confirmThrows();
    }

    this.dartScores.push(valueString);

    if (this.hasCurrentPlayerWon()) {
      this.winningPlayer = this.currentPlayer;
      alert(`${this.players[this.currentPlayer].name} has won!!!`);
      return;
    }
  }

}
