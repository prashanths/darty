import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../player/player.service';
import { Player } from '../player/player.interface';
import * as _ from 'lodash';

@Component({
  selector: 'app-knockout-game',
  templateUrl: './knockout-game.component.html',
  styleUrls: ['./knockout-game.component.scss']
})
export class KnockoutGameComponent implements OnInit {


  public players: Player[];


  public currentHighScore = 20;
  public currentPlayer = 0;
  public playerLives: any;



  /** Scores for the current go. */
  public dartScores = [];



  public winningPlayer?: number;

  constructor(public readonly playerService: PlayerService) { }


  ngOnInit() {
    this.resetGame();
  }


  public get dartScoresSum() {
    return _.sum(this.dartScores.map(v => this.parseValueString(v)));
  }


  public resetGame() {
    this.players = this.playerService.getPlayers();
    this.currentPlayer = 0;
    this.dartScores = [];
    this.winningPlayer = undefined;
    this.playerLives = _.fromPairs(_.range(this.players.length).map(v => {
      return [v, 3];
    }));
  }

  public undoThrow = () => {
    this.dartScores = this.dartScores.slice(0, this.dartScores.length - 1);
  }

  public confirmThrows = () => {
    const currentTotal = this.dartScoresSum;
    if (currentTotal <= this.currentHighScore) {
      this.playerLives[this.currentPlayer] = this.playerLives[this.currentPlayer] - 1;
      // check for the win condition...

      const alivePlayers = Object.keys(this.playerLives).filter(idx => this.playerLives[idx] > 0);
      if (alivePlayers.length === 1) {
        this.winningPlayer = parseInt(alivePlayers[0], 0);
        alert(`${this.players[this.winningPlayer].name} has won!!!`);
      }
      this.currentHighScore = this.currentHighScore / 2;
    } else {
      this.currentHighScore = currentTotal;
    }
    this.dartScores = [];
    this.currentPlayer = (this.currentPlayer + 1) % this.players.length;
  }


  public valueClicked(valueString: string) {
    if (this.winningPlayer) {
      return;
    }

    if (this.dartScores.length === 3) {
      this.confirmThrows();
    }

    this.dartScores.push(valueString);
  }


  public parseValueString(valueString: string) {
    const valueParts = valueString.match(/(t|d|b|s)(\d+)/);
    const numValue = parseInt(valueParts[2], 10);
    const multiplierValue = valueParts[1];

    switch (multiplierValue) {
      case 't': return 3 * numValue;
      case 'd': return 2 * numValue;
      case 'b':
      case 's': return numValue;
      default: throw Error('Unknown value modifier.');
    }
  }

}
