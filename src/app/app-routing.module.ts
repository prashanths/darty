import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameSelectionComponent } from './game-selection/game-selection.component';
import { BasicGameComponent } from './basic-game/basic-game.component';
import { HareAndHoundsGameComponent } from './hare-and-hounds-game/hare-and-hounds-game.component';
import { KnockoutGameComponent } from './knockout-game/knockout-game.component';

const routes: Routes = [

  {
    path: '',
    component: GameSelectionComponent,
    pathMatch: 'full',
  },
  {
    path: 'basic',
    component: BasicGameComponent,
  },
  {
    path: 'hare-and-hounds',
    component: HareAndHoundsGameComponent,
  },
  {
    path: 'knockout',
    component: KnockoutGameComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
