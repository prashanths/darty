import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-quicknav',
  templateUrl: './quicknav.component.html',
  styleUrls: ['./quicknav.component.scss']
})
export class QuicknavComponent {

  @Input() subtitle: string;

  constructor() { }

}
