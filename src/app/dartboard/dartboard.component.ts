import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-dartboard',
  templateUrl: './dartboard.component.html',
  styleUrls: ['./dartboard.component.scss']
})
export class DartboardComponent {

  /**
   * Key/Value list of item states.
   */
  @Input() valueStates: any;
  @Output() valueClicked = new EventEmitter();

  constructor() {}

  public clickValue(val: string) {
    this.valueClicked.emit(val);
  }


}
