

/**
 * Parses a string based value (s1, d5, b25, etc) into a numeric score value.
 * @param valueString The value string from the dartboard.
 */
export function parseValueString(valueString: string) {
  const valueParts = valueString.match(/(t|d|b|s)(\d+)/);
  const numValue = parseInt(valueParts[2], 10);
  const multiplierValue = valueParts[1];

  switch (multiplierValue) {
    case 't': return 3 * numValue;
    case 'd': return 2 * numValue;
    case 'b':
    case 's': return numValue;
    default: throw Error('Unknown value modifier.');
  }
}
